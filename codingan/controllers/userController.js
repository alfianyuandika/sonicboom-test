const { item, transaction, user } = require("../models");

class UserController {
  async getAll(req, res) {
    try {
      let data = await user.find();

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }
}

module.exports = new UserController();
