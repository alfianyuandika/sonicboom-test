const { item, transaction, user } = require("../models");

class ItemController {
  async getAll(req, res) {
    try {
      let data = await item.find().populate("pemasok");

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }

  async create(req, res) {
    try {
      // Create item data
      let data = await item.create(req.body);

      // If success
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
     
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }
}

module.exports = new ItemController();
