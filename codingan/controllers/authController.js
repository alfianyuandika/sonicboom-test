const jwt = require("jsonwebtoken"); // import jsonwebtoken

class AuthController {
  async getToken(req, res) {
    try {
      const body = {
        id: req.user._id,
        email: req.user.email,
      };

      const token = jwt.sign(body, process.env.JWT_SECRET, {
        expiresIn: "60d",
      });

      const name = req.body.name;
      const email = req.user.email;

      return res.status(200).json({
        message: "success",
        token,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }
}

module.exports = new AuthController();
