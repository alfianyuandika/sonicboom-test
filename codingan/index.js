require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

const fs = require("fs");
const path = require("path");

// Express
const express = require("express");


// Import routes
const authRoutes = require("./routes/authRoutes");
const itemRoutes = require("./routes/itemRoutes");
const userRoutes = require("./routes/userRoutes");
const transactionRoutes = require("./routes/transactionRoutes");

// Make express app
const app = express();

// Body-parser to read req.body
app.use(express.json()); // Enable req.body JSON type
app.use(
  express.urlencoded({
    extended: true,
  })
); // Support urlencode body

// To read form-data request
// app.use(fileUpload());

// Set static file directory
app.use(express.static("public"));

// Make routes
app.use("/auth", authRoutes);
app.use("/user", userRoutes)
app.use("/item", itemRoutes);
app.use("/transaction", transactionRoutes);

const PORT = 3000 || process.env.PORT;
app.listen(PORT, () => console.log(`server running on:${PORT}`));

module.exports = app

//pagination, populate
