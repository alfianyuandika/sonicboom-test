const crypto = require("crypto");
const path = require("path");
const mongoose = require("mongoose");
const validator = require("validator");
const { item, transaction, user } = require("../../models");

exports.create = async (req, res, next) => {
  try {
    // Initialita
    let errors = [];

    // If params error
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    let findData = await Promise.all([
      item.findOne({ itemID: req.body.itemID }),
      item.findOne({ itemName: req.body.itemName }),
    ]);

    // If data item is exist
    if (findData[0]) {
      errors.push("ID item already exists");
    }

    if (findData[1]) {
      errors.push("item name already exists");
    }

    // // Check price is number
    if (!validator.isCurrency(req.body.price)) {
      errors.push("price must be a number");
    }

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
