const mongoose = require("mongoose");
const validator = require("validator");
const { item, transaction, user } = require("../../models");

exports.getOne = (req, res, next) => {
  try {
    // Check parameter is valid or not
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json({
        message:
          "Parameter is not valid and must be 24 character & hexadecimal",
      });
    }

    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.create = async (req, res, next) => {
  try {
    let errors = [];

    // Find item and user
    let findData = await Promise.all([
      item.findOne({ itemID: req.body.itemID }),
      user.findOne({ _id: req.body.customerID }),
    ]);

    // if item not found
    if (!findData[0]) {
      errors.push("Item not found");
    }

    // if user not found
    if (!findData[1]) {
      errors.push("User not found");
    }

    // Check is jumlah numeric?
    if (!validator.isNumeric(req.body.qty)) {
      errors.push("Jumlah must be a number");
    }

    // If errors
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // Calculate total
    req.body.item = findData[0];
    req.body.customer = findData[1];
    req.body.subTotal = eval(findData[0].price * req.body.qty);
    req.body.tax = eval(0.1 * req.body.subTotal);
    req.body.total = eval(req.body.subTotal + req.body.tax);

    // Go to next
    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.update = async (req, res, next) => {
  try {
    let errors = [];

    // Check parameter id is valid or not
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      errors.push(
        "id_transaction is not valid and must be 24 character & hexadecimal"
      );
    }

    // Check id_item is valid or not
    if (!mongoose.Types.ObjectId.isValid(req.body.itemID)) {
      errors.push(
        "id_item is not valid and must be 24 character & hexadecimal"
      );
    }

    // Check id_user is valid or not
    if (!mongoose.Types.ObjectId.isValid(req.body.customerID)) {
      errors.push(
        "id_user is not valid and must be 24 character & hexadecimal"
      );
    }

    // If the parameters is not valid it will go here
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // Find item, user and transaction
    let findData = await Promise.all([
      item.findOne({ itemID: req.body.itemID }),
      user.findOne({ _id: req.body.customerID }),
      transaction.findOne({ _id: req.params.id }),
    ]);

    // if item not found
    if (!findData[0]) {
      errors.push("Item not found");
    }

    // If user not found
    if (!findData[1]) {
      errors.push("User not found");
    }

    // If transaction not found
    if (!findData[2]) {
      errors.push("Transaction not found");
    }

    // Check jumlah is numeric
    if (!validator.isNumeric(req.body.jumlah)) {
      errors.push("Jumlah must be a number");
    }

    // If error
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // Calculate total
    req.body.item = findData[0];
    req.body.customer = findData[1];
    req.body.subTotal = eval(findData[0].price * req.body.qty);
    req.body.tax = eval(0.1 * req.body.subTotal);
    req.body.total = eval(req.body.subTotal + req.body.tax);

    // Go to next
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    let errors = [];

    // Check params is valid or not
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      errors.push(
        "id_transaction is not valid and must be 24 character & hexadecimal"
      );
    }

    // If params error
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // Find one transaction
    let data = await transaction.findOne({ _id: req.params.id });

    // If transaction not found
    if (!data) {
      errors.push("Transaction not found");
    }

    // If error
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // Go to next
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
