const mongoose = require("mongoose"); // Import mongoose
const mongooseDelete = require("mongoose-delete"); // Import mongoose-delete

const ItemSchema = new mongoose.Schema(
  {
    itemID: {
      type: String,
      unique: true,
      required: true,
    },
    itemName: {
      type: String,
      unique: true,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
  },
  {
    // Enable timestamps
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true }, // Enable getter
  }
);

// Enable soft delete
ItemSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("item", ItemSchema, "item"); // Export Item models
