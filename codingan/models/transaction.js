const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const TransactionSchema = new mongoose.Schema(
  {
    itemID: [
      {
        type: mongoose.Schema.Types.Mixed,
        required: true,
      },
    ],
    customerID: {
      type: mongoose.Schema.ObjectId,
      ref: "user",
      required: true,
    },
    qty: {
      type: Number,
      required: true,
    },
    subTotal: {
      type: Number,
    },
    tax: {
      type: Number,
    },
    total: {
      type: Number,
    },
  },
  {
    // Enable timestamps
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

// Enable soft delete
TransactionSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model(
  "transaction",
  TransactionSchema,
  "transaction"
); // Export transaction models
