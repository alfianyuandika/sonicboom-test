const express = require("express");

// Import validator
const transactionValidator = require("../middlewares/validators/transactionValidator");

// Import controller
const transactionController = require("../controllers/transactionController");

// Import auth (middleware)
const auth = require("../middlewares/auth");

// Make router
const router = express.Router();

router
  .route("/")
  .get(auth.allUser, transactionController.getAll)
  .post(/*auth.staff,*/ transactionValidator.create, transactionController.create);

router
  .route("/:id")
  .get(auth.allUser, transactionValidator.getOne, transactionController.getOne)
  .put(auth.staff, transactionValidator.update, transactionController.update)
  .delete(auth.staff, transactionValidator.delete, transactionController.delete);

// Get all transaction data
// router.get("/", auth.user, transactionController.getAll);

// Get one transaction
// router.get(
//   "/:id",
//   auth.user,
//   transactionValidator.getOne,
//   transactionController.getOne
// );

// Create transaction
// router.post(
//   "/",
//   auth.admin,
//   transactionValidator.create,
//   transactionController.create
// );

// Update transaction
// router.put(
//   "/:id",
//   auth.admin,
//   transactionValidator.update,
//   transactionController.update
// );

// Delete transaction
// router.delete(
//   "/:id",
//   auth.admin,
//   transactionValidator.delete,
//   transactionController.delete
// );

module.exports = router;
