const express = require("express");

// Import controller
const userController = require("../controllers/userController");

// Import auth (middleware)
const auth = require("../middlewares/auth");

// Make router
const router = express.Router();

router.route("/").get(userController.getAll);
// router.route("/").post(userController.signUp);


module.exports = router;
