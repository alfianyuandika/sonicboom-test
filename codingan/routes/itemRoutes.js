const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import middlewares
const itemValidator = require("../middlewares/validators/itemValidator");

// Import controller
const itemController = require("../controllers/itemController");

router.get("/", itemController.getAll);

// If POST (/item)
// Then, go to transactionValidator.create
// If in the transactionValidator.create can run the next(), it will go to transactionController.create
router.post("/", itemValidator.create, itemController.create);

module.exports = router; // Export router
