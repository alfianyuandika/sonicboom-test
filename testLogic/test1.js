

const arr = [1,5,7,9,11];
const findMissingNumber = (arr = []) => {
  let length  = arr.length;
  console.log(length)
  let diff1 = arr[1] - arr[0];
  let diff2 = arr[length - 1] - arr[length - 2];
  if (diff1 !== diff2) {
    if (diff1 == 2 * diff2) {
      return arr[0] + diff2;
    } else {
      return arr[length - 1] - diff1;
    }
  }
  for (let i = 1; i < length - 2; i++) {
    if (arr[i + 1] - arr[i] != diff2) {
      return arr[i+1] - diff2;
    }
  }
  return arr[0];
};
console.log(findMissingNumber(arr));
