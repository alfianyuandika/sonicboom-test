function isPrime(num) {
  let result = true;
  if (num < 2) {
    return false;
  } else if (num === 2) {
    return true;
  }
  if (num % 2 === 1) { //nyari bilangan ganjil
    for (let i = 2; i <= Math.ceil(Math.sqrt(num)); i++) {
      if (num % i === 0) {
        result = false;
        break;
      }
    }
  } else { //bilangan genap
    result = false;
  }
  return result;
}

function getNumberOfPrimes (N) {
    let prime = [];
    for (let i = 1; i <= N; i ++) {
      let result = isPrime(i);
      if (result === true) {
        prime.push(i);
      }
    }
    console.log(prime)
    console.log(prime.length)
    return prime.length
}
console.log(getNumberOfPrimes(100));
console.log(getNumberOfPrimes(1000000));


